@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>
                <div class="panel-body">
                    Welcome <h3 style="display: inline;text-transform: capitalize; font-weight: 900">{{\Auth::user()->name}}</h3>
                    <br>
                    You have role of 

                    @foreach(\Auth::user()->roles as $role) 

                        <h3 style="display: inline;text-transform: capitalize; font-weight: 900">
                            {{$role->name}} 
                        </h3>

                    @endforeach
                    
                    <br>

                    You have permission of 

                    @foreach(\Auth::user()->roles as $role)
                        @foreach($role->permissions as $permission)

                         <h3 style="display: inline;text-transform: capitalize; font-weight: 900">
                            {{$permission->name}}
                         </h3>

                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                @if(\Auth::user()->hasRole('admin'))
                <a href="/roles">manage roles</a><br>
                @endif
                    
                    <a href="/customers">manage customers</a><br>
                    <a href="/items">manage items </a><br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
