@extends('layouts.app')

@section('header_meta_section')
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">All Customer Table <span class="pull-right"><a href="/customers/create">New Customer</a></span></div>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <div class="panel-body">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>customer name</th>
                                <th>telephone</th>
                                <th>address</th>
                                <th>gender</th>
                                <th>purchased</th>
                                <th>status</th>
                                <th>city</th>
                                <th>country</th>
                                <th>action</th>

                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>customer name</th>
                                <th>telephone</th>
                                <th>address</th>
                                <th>gender</th>
                                <th>purchased</th>
                                <th>status</th>
                                <th>city</th>
                                <th>country</th>
                                <th>action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($customers as $customer)
                            <tr>
                                <td>{{$customer->id}}</td>
                                <td>{{$customer->customer_name}}</td>
                                <td>{{$customer->telephone}}</td>
                                <td>{{$customer->address}}</td>
                                <td>{{$customer->gender}}</td>
                                <td>{{$customer->purchased}}</td>
                                <td>{{$customer->status}}</td>
                                <td>{{$customer->city}}</td>
                                <td>{{$customer->country}}</td>
                                <td>
                                    <a href="/customers/{{$customer->id}}">View</a><br>
                                    <a href="/customers/{{$customer->id}}/edit">Edit</a><br>
                                    <form method="post" action="/customers/{{$customer->id}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="id" value="{{$customer->id}}">
                                        <button type="submit">Delete</button>
                                    </form>

                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

@section('footer_meta_section')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>

@endsection
