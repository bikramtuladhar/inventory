@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Item Creation <span class="pull-right">
                            <a href="/roles">View All</a>
                        </span></div>
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <div class="panel-body">
                      
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/roles') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('rolename') ? ' has-error' : '' }}">
                                <label for="rolename" class="col-md-4 control-label">Role Name</label>

                                <div class="col-md-6">
                                    <input id="rolename" type="text" class="form-control" name="rolename" value="{{ old('rolename') }}" autofocus required>

                                    @if ($errors->has('rolename'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rolename') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('rolelabel') ? ' has-error' : '' }}">
                                <label for="rolelabel" class="col-md-4 control-label">Role rolelabel</label>

                                <div class="col-md-6">
                                    <input id="rolelabel" type="text" class="form-control" name="rolelabel" value="{{ old('rolelabel') }}" autofocus required>

                                    @if ($errors->has('rolelabel'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rolelabel') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            @inject('permisson','App\Repositories\PermisionRepository')

                            <div class="form-group{{ $errors->has('permission') ? ' has-error' : '' }}">
                                <label for="permission" class="col-md-4 control-label">Permission</label>

                                <div class="col-md-6" id="permission">

                                <select class="form-control" name="permission[]" id="permission" onchange="showfield(this.options[this.selectedIndex].value)" multiple="multiple">
                                    @foreach($permisson->totalPermission() as $permission)
                                    <option value="{{$permission->name}}">{{$permission->name}}</option>
                                    @endforeach
                                    <option value="new">Create new permission</option>
                                </select>

                                    @if ($errors->has('permission'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('permission') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

            <script type="text/javascript">
                function showfield(name){
                  if(name=='new')document.getElementById('permission').innerHTML='<input id="permission" type="text" class="form-control" name="permission[]" placeholder="new permission name" value="{{ old('permission') }}" autofocus required>';
                }
            </script>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create New Role
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
