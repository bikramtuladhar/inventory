@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Detail View of item <b>{{$item->id}}</b> <span class="pull-right">
                                    <form style="display: inline" method="post" action="/items/{{$item->id}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="id" value="{{$item->id}}">
                                        <button type="submit">Delete</button>
                                    </form>
                            <a href="/items/{{$item->id}}/edit">Edit Data</a>
                            <a href="/items">View All</a>
                        </span></div>
@if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <div class="panel-body">
                        <h2>item name: {{$item->name}}</h2>
                        <h2>item unit: {{$item->unit}}</h2>
                        <h2>item price: {{$item->price}}</h2>
                        <h2>item quantity: {{$item->quantity}}</h2>
                        <h2>item status: {{$item->status}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
