@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Detail View of Role <b>{{$role->id}}</b> <span class="pull-right">
                                    <form style="display: inline" method="post" action="/roles/{{$role->id}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="id" value="{{$role->id}}">
                                        <button type="submit">Delete</button>
                                    </form>
                            <a href="/roles/{{$role->id}}/edit">Edit Data</a>
                            <a href="/roles">View All</a>
                        </span></div>
@if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <div class="panel-body">
                        <h2>Role name: {{$role->name}}</h2>
                        <h2>Role label: {{$role->label}}</h2>
                        <h2>Permission: @foreach($role->permissions as $permission) <b>{{$permission->name}}</b>@endforeach</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
