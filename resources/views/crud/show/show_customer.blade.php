@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Detail View of Customer <b>{{$customer->id}}</b> <span class="pull-right">
                                    <form style="display: inline" method="post" action="/customers/{{$customer->id}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="id" value="{{$customer->id}}">
                                        <button type="submit">Delete</button>
                                    </form>
                            <a href="/customers/{{$customer->id}}/edit">Edit Data</a>
                            <a href="/customers">View All</a>
                        </span></div>
@if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <div class="panel-body">
                        <h2>Customer name: {{$customer->customer_name}}</h2>
                        <h2>Customer telephone: {{$customer->telephone}}</h2>
                        <h2>Customer address: {{$customer->address}}</h2>
                        <h2>Customer gender: {{$customer->gender}}</h2>
                        <h2>Customer purchased: {{$customer->purchased}}</h2>
                        <h2>Customer status: {{$customer->status}}</h2>
                        <h2>Customer city: {{$customer->city}}</h2>
                        <h2>Customer country: {{$customer->country}}</h2>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
