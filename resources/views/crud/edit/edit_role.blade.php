@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
         
                    <div class="panel-heading">Item Created <span class="pull-right">
                                    <form style="display: inline" method="post" action="/roles/{{$role->id}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="id" value="{{$role->id}}">
                                        <button type="submit">Delete</button>
                                    </form>
                            <a href="/roles/{{$role->id}}">View Data</a>
                            <a href="/roles">View All</a>
                        </span></div>
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <div class="panel-body">

                        <form class="form-horizontal" role="form" method="POST" action="/roles/{{$role->id}}">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Role Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value=@if(old('name'))"{{old('name')}}" @else "{{$role->name}}" @endif autofocus required>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('label') ? ' has-error' : '' }}">
                                <label for="label" class="col-md-4 control-label">Role label</label>

                                <div class="col-md-6">
                                    <input id="label" type="text" class="form-control" name="label" value=@if(old('name'))"{{old('label')}}" @else "{{$role->label}}" @endif required>

                                    @if ($errors->has('label'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('label') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            @inject('users','App\Repositories\UserRepository')

                            <div class="form-group{{ $errors->has('users') ? ' has-error' : '' }}">
                                <label for="users" class="col-md-4 control-label">Users</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="users[]" id="users" multiple="multiple">
                                    @foreach($users->totalUser() as $user)
                                    <option value="{{$user->email}}">{{$user->email}}</option>
                                    @endforeach
                                </select>
                                    @if ($errors->has('users'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('users') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                             @inject('permisson','App\Repositories\PermisionRepository')

                            <div class="form-group{{ $errors->has('permission') ? ' has-error' : '' }}">
                                <label for="permission" class="col-md-4 control-label">Permission</label>

                                <div class="col-md-6" id="permission">

                                <select class="form-control" name="permission[]" id="permission" onchange="showfield(this.options[this.selectedIndex].value)" multiple="multiple">
                                    @foreach($permisson->totalPermission() as $permission)
                                    <option value="{{$permission->name}}">{{$permission->name}}</option>
                                    @endforeach
                                    <option value="new">Create new permission</option>
                                </select>

                                    @if ($errors->has('permission'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('permission') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

            <script type="text/javascript">
                function showfield(name){
                  if(name=='new')document.getElementById('permission').innerHTML='<input id="permission" type="text" class="form-control" name="permission[]" placeholder="new permission name" value="{{ old('permission') }}" autofocus required>';
                }
            </script>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update Item
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
