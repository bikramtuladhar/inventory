@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Customer Created <span class="pull-right">
                                    <form style="display: inline" method="post" action="/customers/{{$customer->id}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="id" value="{{$customer->id}}">
                                        <button type="submit">Delete</button>
                                    </form>
                            <a href="/customers/{{$customer->id}}">View Data</a>
                            <a href="/customers">View All</a>
                        </span></div>
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <div class="panel-body">
                       
                        <form class="form-horizontal" role="form" method="POST" action="/customers/{{$customer->id}}">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group{{ $errors->has('customer_name') ? ' has-error' : '' }}">
                                <label for="customer_name" class="col-md-4 control-label">Customer Name</label>

                                <div class="col-md-6">
                                    <input id="customer_name" type="text" class="form-control" name="customer_name" value=@if(old('user_name'))"{{old('user_name')}}" @else "{{$customer->customer_name}}" @endif autofocus>

                                    @if ($errors->has('customer_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="col-md-4 control-label">Address</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control" placeholder="{{$customer->address}}" name="address" value=@if(old('address'))"{{old('address')}}" @else "{{$customer->address}}" @endif>

                                    @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label for="gender" class="col-md-4 control-label">Gender</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="gender" id="gender">
                                        <option @if($customer->gender==='male') selected @endif value="male">male</option>
                                        <option @if($customer->gender==='female') selected @endif value="female">female</option>
                                    </select>
                                    @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">Status</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="status" id="status">
                                        <option @if($customer->status===0) selected @endif value="0">Inactive</option>
                                        <option @if($customer->status===1) selected @endif value="1">Active</option>
                                    </select>
                                    @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                                <label for="telephone" class="col-md-4 control-label">Telephone</label>

                                <div class="col-md-6">
                                    <input id="telephone" type="text" class="form-control" name="telephone" value=@if(old('telephone'))"{{old('telephone')}}" @else "{{$customer->telephone}}" @endif>

                                    @if ($errors->has('telephone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('purchased') ? ' has-error' : '' }}">
                                <label for="purchased" class="col-md-4 control-label">Purchased</label>

                                <div class="col-md-6">
                                    <input id="purchased" type="number" placeholder="{{$customer->purchased}}" class="form-control" name="purchased" value=@if(old('purchased'))"{{old('purchased')}}" @else "{{$customer->purchased}}" @endif>

                                    @if ($errors->has('purchased'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('purchased') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                <label for="city" class="col-md-4 control-label">City</label>

                                <div class="col-md-6">
                                    <input id="city" placeholder="{{$customer->city}}" type="text" class="form-control" name="city" value=@if(old('city'))"{{old('city')}}" @else "{{$customer->city}}" @endif>

                                    @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                <label for="country" class="col-md-4 control-label">Country</label>

                                <div class="col-md-6">
                                    <input id="country" type="text" placeholder="{{$customer->country}}" class="form-control" name="country" value=@if(old('country'))"{{old('country')}}" @else "{{$customer->country}}" @endif>

                                    @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update Customer
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
