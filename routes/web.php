<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index');

Route::get('users/activation/{token}', 'Auth\LoginController@activateUser')->name('user.activate');
Route::resource('customers', 'CustomerController');
Route::resource('items', 'ItemController');
Route::resource('roles', 'RoleController');
