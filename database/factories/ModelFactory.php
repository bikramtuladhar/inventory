<?php

use App\Services\RandomGenerator;

$factory->define(App\User::class, function (Faker\Generator $faker) {

    static $password;
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'customer_name' => $faker->name,
        'telephone' => $faker->phoneNumber,
        'address' => $faker->address,
        'gender'  => RandomGenerator::randomGen(['male','female']),
        'purchased' => $faker->biasedNumberBetween(),
        'status'  => mt_rand(0, 1),
        'city' => $faker->city,
        'country' => $faker->country,
        'user_id' => mt_rand(1, 50)
    ];
});

$factory->define(App\Item::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'unit' => RandomGenerator::randomGen(['pound','liter','kg']),
        'price' => mt_rand(1, 5000),
        'quantity' => mt_rand(1, 50),
        'status' => mt_rand(0, 1),
        'user_id' => mt_rand(1, 50)
    ];
});
