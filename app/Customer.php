<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['customer_name',
        'telephone',
        'address',
        'gender',
        'purchased',
        'status',
        'city',
        'country'
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
