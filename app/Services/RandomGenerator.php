<?php
namespace App\Services;

class RandomGenerator
{
    public static function randomGen(array $input)
    {
          $random_key = array_rand($input);
          $random_value = $input[$random_key];
          return $random_value;
    }
}
