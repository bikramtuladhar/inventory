<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;
use Validator;
use App\User;
use App\Role;
use App\Permission;
use App\Services\ActivationService;

class InventorySetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inventory:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Super Admin for Inventory and setup Inventory App';

    protected $validator;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ActivationService $activationService)
    {
        $this->activationService = $activationService;
        parent::__construct();
    }

    public function validationRule()
    {
        return [
          'name' => 'required|max:255',
          'email' => 'required|email|max:255|unique:users',
          'password' => 'required|min:6|confirmed',
        ];
    }

    public function createUser()
    {
        $user = null;
        $input=[];
        $this->info('!! Setup Start for SuperUser !!');
        $exitCode = Artisan::call('migrate:refresh', ['--force' => true,]);
        if (!$exitCode) {
            $this->info('Migration Finish !!');
        }

        $input['name'] = $this->ask('What is your name ?', 'Bikram Tuladhar');
        $input['email'] = $this->ask('What is your email?', 'bikramtuladhar2011@gmail.com');
        $input['password'] = $this->secret('What is your password ?');
        $input['password_confirmation'] = $this->secret('verify password ?');

        $v = Validator::make($input, $this->validationRule());

        if ($v->fails()) {
            foreach ($v->messages()->all() as $error) {
                $this->error($error);
                exit;
            }
        }
        if ($this->confirm('Do you wish to continue?')) {
            $user = User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => bcrypt($input['password']),
            ]);
            $this->activationService->sendActivationMail($user);
            $this->info('Check email for Email validation !!');
            $this->info('User Created. !!');
            return $user;
        }
    }

    public function assignRole(User $user)
    {
        $new_role= new Role;
        $new_role->name = 'admin';
        $new_role->label = 'admin';
        $user->roles()->save($new_role);
        $this->info('Current User assigned to Admin Role !');
        return $new_role;
    }

    public function assignPermission(Role $role)
    {
        $this->info('Settingup Permmsion for Admin Role !');

        $permission_list=['create','edit','view','delete'];

        foreach ($permission_list as $value) {
            $new_permission = new Permission;
            $new_permission->name = $value;
            $new_permission->label = $value;
            $role->givePermissionTo($new_permission);
            $this->info('Admin role have '.$value.' permission !');
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = $this->createUser();
        
        $role = $this->assignRole($user);
        $this->assignPermission($role);
        $this->info('setup complete !!');
    }
}
