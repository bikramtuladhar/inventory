<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name' => 'required|max:255',
            'telephone' => 'required',
            'address' => 'required|max:255',
            'gender' => 'required',
            'purchased' => 'regex:/^\d*(\.\d{1,4})?$/',
            'status' => 'boolean',
            'city' => 'required|max:100',
            'country' => 'required|max:100'
        ];
    }
}
