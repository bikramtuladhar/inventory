<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdateCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name' => 'max:255',
            'telephone' => 'max:20',
            'address' => 'max:255',
            'gender' => '',
            'purchased' => 'regex:/^\d*(\.\d{1,4})?$/',
            'status' => 'boolean',
            'city' => 'max:100',
            'country' => 'max:100'
        ];
    }
}
