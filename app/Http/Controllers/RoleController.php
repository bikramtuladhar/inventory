<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRole;
use App\Http\Requests\UpdateRole;
use App\Role;
use App\Permission;
use App\User;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index()
    {
        $roles=Role::all();
        return view('crud.index.all_role', compact('roles'));
    }

    public function create()
    {
        $this->authorize('create', Role::class);
        return view('crud.create.create_role');
    }

    public function store(StoreRole $request)
    {
        $new_role= new Role;
        $this->authorize('create', Role::class);
        $new_role->name= $request->rolename;
        $new_role->label= $request->rolelabel;
        $new_role->save();
        if ($request->has('permission')) {
            foreach ($request->permission as $key => $value) {
                $permission = Permission::firstOrNew(['name'=> $value]);
                $new_role->givePermissionTo($permission);
            }
        }
        return back()->with('status', 'role created sucessfuly');
    }

    public function show($id)
    {
        $role = Role::findOrFail($id);
        $this->authorize('view', $role);
        return view('crud.show.show_role', compact('role'));
    }

    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $this->authorize('update', $role);
        return view('crud.edit.edit_role', compact('role'));
    }

    public function update(UpdateRole $request, $id)
    {
        $role = Role::findOrFail($id);
        $this->authorize('update', $role);
        $role->name=$request->name;
        $role->label=$request->label;
        $role->save();
        if ($request->has('permission')) {
            foreach ($request->permission as $key => $value) {
                $permission = Permission::firstOrNew(['name'=> $value]);
                if (!$role->permissions->contains('name', $value)) {
                    $role->givePermissionTo($permission);
                }
            }
        }
        if ($request->has('users')) {
            foreach ($request->users as $key => $value) {
                $user = User::where('email', $value)->first();
                if (!$user->hasRole($request->name)) {
                    $user->assignRole($request->name);
                }
            }
        }
        return back()->with('status', 'Role Update sucessfully');
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $this->authorize('delete', $role);
        Role::destroy($id);
        return redirect('/roles')->with('status', 'Role deleted');
    }
}
