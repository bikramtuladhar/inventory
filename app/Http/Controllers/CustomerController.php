<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\StoreCustomer;
use App\Http\Requests\UpdateCustomer;
use Auth;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::all();
        return view('crud.index.all_customer', compact('customers'));
    }

    public function create()
    {
        $this->authorize('create', Customer::class);
        return view('crud.create.create_customer');
    }

    public function store(StoreCustomer $request)
    {
        $this->authorize('create', Customer::class);
        $new_customer= new Customer;
        $new_customer->fill($request->toArray());
        Auth::user()->customer()->save($new_customer);
        return back()->with('status', 'New customer Added');
    }

    public function show($id)
    {
        $customer= Customer::findOrFail($id);
        $this->authorize('view', $customer);
        return view('crud.show.show_customer', compact('customer'));
    }

    public function edit($id)
    {
        $customer= Customer::findOrFail($id);
        $this->authorize('update', $customer);
        return view('crud.edit.edit_customer', compact('customer'));
    }

    public function update(UpdateCustomer $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $this->authorize('update', $customer);
        $customer->update($request->toArray());
        return back()->with('status', 'customer Update Succesfully');
    }
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $this->authorize('delete', $customer);
        Customer::destroy($id);
        return redirect('/customers')->with('status', 'Item deleted');
    }
}
