<?php

namespace App\Http\Controllers;

use App\Item;
use App\Http\Requests\StoreItem;
use App\Http\Requests\UpdateItem;
use Auth;

class ItemController extends Controller
{
    public function index()
    {
        $items = Item::all();
        return view('crud.index.all_item', compact('items'));
    }

    public function create()
    {
        $this->authorize('create', Item::class);
        return view('crud.create.create_item');
    }

    public function store(StoreItem $request)
    {
        $this->authorize('create', Item::class);
        $new_item= new Item;
        $new_item->fill($request->toArray());
        Auth::user()->item()->save($new_item);
        return back()->with('status', 'New Item Added');
    }

   
    public function show($id)
    {
        $item= Item::findOrFail($id);
        $this->authorize('view', $item);
        return view('crud.show.show_item', compact('item'));
    }

    public function edit($id)
    {
        $item= Item::findOrFail($id);
        $this->authorize('update', $item);
        return view('crud.edit.edit_item', compact('item'));
    }

    public function update(UpdateItem $request, $id)
    {
        $item = Item::findOrFail($id);
        $this->authorize('update', $item);
        $item->update($request->toArray());
        return back()->with('status', 'Item Update Successfully');
    }

    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        $this->authorize('delete', $item);
        $item->destroy($id);
        return redirect('/items')->with('status', 'Item deleted');
        ;
    }
}
