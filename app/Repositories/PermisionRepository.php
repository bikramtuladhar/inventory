<?php
namespace App\Repositories;

use App\Permission;

class PermisionRepository
{
    public function totalPermission()
    {
        return Permission::all();
    }
}
