<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Permission;
use Illuminate\Database\Eloquent\Model;
use App\Policies\PolicyInterface;
use App\User;

abstract class BasePolicy implements PolicyInterface
{

    use HandlesAuthorization;

    public function view(User $user, Model $model)
    {
        return ($user->hasPermission(Permission::where('name', 'view')->first()) || $user->id=== $model->user_id) ? true : false ;
    }

    public function create(User $user)
    {
        return ($user->hasPermission(Permission::where('name', 'create')->first())) ? true : false ;
    }

    public function update(User $user, Model $model)
    {
        return ($user->hasPermission(Permission::where('name', 'edit')->first()) || $user->id=== $model->user_id) ? true : false ;
    }

    public function delete(User $user, Model $model)
    {
        return ($user->hasPermission(Permission::where('name', 'delete')->first()) || $user->id=== $model->user_id) ? true : false ;
    }
}
