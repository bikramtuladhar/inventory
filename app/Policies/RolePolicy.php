<?php

namespace App\Policies;

use App\Policies\BasePolicy;
use App\User;
use App\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy extends BasePolicy
{
    use HandlesAuthorization;
    public function view(User $user, Model $model)
    {
        return ($user->hasRole('admin')) ? true : false ;
    }

    public function create(User $user)
    {
        return ($user->hasRole('admin')) ? true : false ;
    }

    public function update(User $user, Model $model)
    {
        return ($user->hasRole('admin')) ? true : false ;
    }

    public function delete(User $user, Model $model)
    {
        return ($user->hasRole('admin')) ? true : false ;
    }
}
