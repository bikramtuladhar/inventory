<?php
namespace App\Policies;

use App\User;
use Illuminate\Database\Eloquent\Model;

interface PolicyInterface
{
    public function view(User $user, Model $item);

    public function create(User $user);
 
    public function update(User $user, Model $item);
 
    public function delete(User $user, Model $item);
}
