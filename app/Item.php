<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable=[
                'name',
                'unit',
                'price',
                'quantity',
                'status',
            ];
            
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
